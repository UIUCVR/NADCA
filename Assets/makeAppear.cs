﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class makeAppear : MonoBehaviour {

	// Use this for initialization
	bool offL = false;
	float time = 0;
	float timeScale = 1000f;
	Renderer rend;
	BoxCollider box;

	void Start(){
		rend = gameObject.GetComponent<Renderer>();
		box = gameObject.GetComponent<BoxCollider> ();
	}

	void Update(){
		bool clickL = OVRInput.Get (OVRInput.RawButton.LThumbstick);

		float safety = Time.time - time;
		if (clickL && safety>1f) {
			if (offL) {
//				GameObject tab = GameObject.Instantiate (tablet, GameObject.Find ("hand_left").transform.position, Quaternion.identity);
				Transform target = GameObject.Find ("CenterEyeAnchor").transform; 
//				
//				tab.transform.rotation = Quaternion.LookRotation (tab.transform.position - target.position);
//				//GameObject.Find ("CADManager").GetComponent<loadAssetBundle> ().enabled = true;
//				tab.tag = "tablet";
				Vector3 posHand = GameObject.Find("hand_left").transform.position;
				gameObject.transform.rotation = Quaternion.LookRotation (posHand - target.position);
				gameObject.transform.position = posHand;
				StartCoroutine (scaleUp ());
				gameObject.transform.GetChild (0).gameObject.GetComponent<CanvasGroup> ().alpha = 1;
				gameObject.transform.GetChild (0).gameObject.GetComponent<CanvasGroup> ().interactable = true;
				box.enabled = true;
				rend.enabled = true;
				offL = false;
			} 
			else {


				//GameObject.Find ("CADManager").GetComponent<loadAssetBundle> ().enabled = false;
				StartCoroutine (scaleDown ());
//				rend.enabled = false;
//				box.enabled = false;
//				gameObject.transform.GetChild (0).gameObject.GetComponent<CanvasGroup> ().alpha = 0;
//				gameObject.transform.GetChild (0).gameObject.GetComponent<CanvasGroup> ().interactable = false;

				offL = true;
			}

			time = Time.time;
		}

	}

	IEnumerator scaleUp(){
		float time = 0.25f;
		Vector3 finScale = new Vector3 (0.3f, 0.4f, 0.02f);
		Vector3 origScale = gameObject.transform.localScale;

		float curTime = 0.0f;

		do {
			gameObject.transform.localScale = Vector3.Lerp (origScale, finScale, curTime / time);
			curTime += Time.deltaTime;
			yield return null;
		} while(curTime <= time);
	}

	IEnumerator scaleDown(){
		float time = 0.25f;
		Vector3 origScale = gameObject.transform.localScale;
		Vector3 finScale = Vector3.zero;

		float curTime = 0.0f;

		do {
			gameObject.transform.localScale = Vector3.Lerp (origScale, finScale, curTime / time);
			curTime += Time.deltaTime;
			yield return null;
		} while(curTime <= time);

		rend.enabled = false;
		box.enabled = false;
		gameObject.transform.GetChild (0).gameObject.GetComponent<CanvasGroup> ().alpha = 0;
		gameObject.transform.GetChild (0).gameObject.GetComponent<CanvasGroup> ().interactable = false;
	}
}
