﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class buttonUpdate : MonoBehaviour {
	
	// Update is called once per frame
	public int index;
	void Update () {
		int currStatus = listStatus.storage [index];
		if (currStatus == 1) {
			gameObject.GetComponent<Image>().color = new Color (0, 1, 0, 1);
		}
		else {
			gameObject.GetComponent<Image>().color = new Color (1, 0, 0, 1);
		}
	}

}
