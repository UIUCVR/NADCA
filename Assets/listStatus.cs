﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class listStatus : MonoBehaviour {

	// Use this for initialization
	public static int[] storage;
	void Start () {
		storage = new int[18];
		for (int i = 0; i < storage.Length; i++) {
			storage [i] = 0;
		}
	}

	public void updateButtonStatus(int index){
		int currStat = storage [index];
		if (currStat == 1) {
			storage [index] = 0;
		} else {
			storage [index] = 1;
		}
	}
}
