﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class onClickFuncs : MonoBehaviour {

	// Use this for initialization
	public void oilToggle(Button button){
		GameObject oil = GameObject.Find ("Oil");
		SpriteRenderer rend = oil.GetComponent<SpriteRenderer>();
		Vector2 xz = Random.insideUnitCircle;
		oil.transform.position = new Vector3 (xz.x, 0.001f, xz.y*4f);
		rend.enabled = !rend.enabled;
	}
}
