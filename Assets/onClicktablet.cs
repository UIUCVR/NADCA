﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class onClicktablet : MonoBehaviour {

	//for disabling/abling the tablet
	bool offL = true;
	float time = 0;
	public GameObject tablet;

	void Update(){
		bool clickL = OVRInput.Get (OVRInput.RawButton.LThumbstick);

		float safety = Time.time - time;
		if (clickL && safety>1f) {
			if (offL) {
				GameObject tab = GameObject.Instantiate (tablet, GameObject.Find ("hand_left").transform.position, Quaternion.identity);
				Transform target = GameObject.Find ("CenterEyeAnchor").transform; 
				tab.transform.rotation = Quaternion.LookRotation (tab.transform.position - target.position);
				//GameObject.Find ("CADManager").GetComponent<loadAssetBundle> ().enabled = true;
				tab.tag = "tablet";
				offL = false;
			} 
			else {
				GameObject.Destroy(GameObject.FindGameObjectWithTag("tablet"));
				//GameObject.Find ("CADManager").GetComponent<loadAssetBundle> ().enabled = false;
				offL = true;
			}

			time = Time.time;
		}

	}
}
