﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : MonoBehaviour {

	public void openLoadingWindow(){
		GameObject mainMenu = GameObject.Find ("MainMenu");
		GameObject fileExplorerCanvas = GameObject.Find ("FileExplorerCanvas");
		mainMenu.GetComponent<CanvasGroup> ().alpha = 0f;
		mainMenu.GetComponent<CanvasGroup> ().blocksRaycasts = false;
		mainMenu.GetComponent<CanvasGroup> ().interactable = false;
		fileExplorerCanvas.GetComponent<CanvasGroup> ().interactable = true;
		fileExplorerCanvas.GetComponent<CanvasGroup> ().blocksRaycasts = true;
		fileExplorerCanvas.GetComponent<CanvasGroup> ().alpha = 1f;
	}

	public void backMainMenuFromLoading(){
		GameObject mainMenu = GameObject.Find ("MainMenu");
		GameObject fileExplorerCanvas = GameObject.Find ("FileExplorerCanvas");
		mainMenu.GetComponent<CanvasGroup> ().alpha = 1f;
		mainMenu.GetComponent<CanvasGroup> ().blocksRaycasts = true;
		mainMenu.GetComponent<CanvasGroup> ().interactable = true;
		fileExplorerCanvas.GetComponent<CanvasGroup> ().interactable = false;
		fileExplorerCanvas.GetComponent<CanvasGroup> ().blocksRaycasts = false;
		fileExplorerCanvas.GetComponent<CanvasGroup> ().alpha = 0f;
	}

	public void openTransformWindow(){
		GameObject mainMenu = GameObject.Find ("MainMenu");
		GameObject transformCanvas = GameObject.Find ("TransformCanvas");
		mainMenu.GetComponent<CanvasGroup> ().alpha = 0f;
		mainMenu.GetComponent<CanvasGroup> ().blocksRaycasts = false;
		mainMenu.GetComponent<CanvasGroup> ().interactable = false;
		transformCanvas.GetComponent<CanvasGroup> ().interactable = true;
		transformCanvas.GetComponent<CanvasGroup> ().blocksRaycasts = true;
		transformCanvas.GetComponent<CanvasGroup> ().alpha = 1f;
	}

	public void backMainMenuFromTransform(){
		GameObject mainMenu = GameObject.Find ("MainMenu");
		GameObject transformCanvas = GameObject.Find ("TransformCanvas");
		mainMenu.GetComponent<CanvasGroup> ().alpha = 1f;
		mainMenu.GetComponent<CanvasGroup> ().blocksRaycasts = true;
		mainMenu.GetComponent<CanvasGroup> ().interactable = true;
		transformCanvas.GetComponent<CanvasGroup> ().interactable = false;
		transformCanvas.GetComponent<CanvasGroup> ().blocksRaycasts = false;
		transformCanvas.GetComponent<CanvasGroup> ().alpha = 0f;
	}

	public void openRayleighWindow(){
		GameObject mainMenu = GameObject.Find ("MainMenu");
		GameObject rCanvas = GameObject.Find ("RayleighCanvas");
		mainMenu.GetComponent<CanvasGroup> ().alpha = 0f;
		mainMenu.GetComponent<CanvasGroup> ().blocksRaycasts = false;
		mainMenu.GetComponent<CanvasGroup> ().interactable = false;
		rCanvas.GetComponent<CanvasGroup> ().interactable = true;
		rCanvas.GetComponent<CanvasGroup> ().blocksRaycasts = true;
		rCanvas.GetComponent<CanvasGroup> ().alpha = 1f;
	}

	public void backMainMenuFromRayleigh(){
		GameObject mainMenu = GameObject.Find ("MainMenu");
		GameObject rCanvas = GameObject.Find ("RayleighCanvas");
		mainMenu.GetComponent<CanvasGroup> ().alpha = 1f;
		mainMenu.GetComponent<CanvasGroup> ().blocksRaycasts = true;
		mainMenu.GetComponent<CanvasGroup> ().interactable = true;
		rCanvas.GetComponent<CanvasGroup> ().interactable = false;
		rCanvas.GetComponent<CanvasGroup> ().blocksRaycasts = false;
		rCanvas.GetComponent<CanvasGroup> ().alpha = 0f;
	}

	public void openSizeWindow(){
		GameObject mainMenu = GameObject.Find ("MainMenu");
		GameObject pCanvas = GameObject.Find ("PointCanvas");
		mainMenu.GetComponent<CanvasGroup> ().alpha = 0f;
		mainMenu.GetComponent<CanvasGroup> ().blocksRaycasts = false;
		mainMenu.GetComponent<CanvasGroup> ().interactable = false;
		pCanvas.GetComponent<CanvasGroup> ().interactable = true;
		pCanvas.GetComponent<CanvasGroup> ().blocksRaycasts = true;
		pCanvas.GetComponent<CanvasGroup> ().alpha = 1f;
	}

	public void backMainMenuFromSize(){
		GameObject mainMenu = GameObject.Find ("MainMenu");
		GameObject pCanvas = GameObject.Find ("PointCanvas");
		mainMenu.GetComponent<CanvasGroup> ().alpha = 1f;
		mainMenu.GetComponent<CanvasGroup> ().blocksRaycasts = true;
		mainMenu.GetComponent<CanvasGroup> ().interactable = true;
		pCanvas.GetComponent<CanvasGroup> ().interactable = false;
		pCanvas.GetComponent<CanvasGroup> ().blocksRaycasts = false;
		pCanvas.GetComponent<CanvasGroup> ().alpha = 0f;
	}
}
